mobj_attribute_names = [
    'thingid',
    'spawnstate',
    'spawnhealth',
    'seestate',
    'seesound',
    'reactiontime',
    'attacksound',
    'painstate',
    'painchance',
    'painsound',
    'meleestate',
    'missilestate',
    'deathstate',
    'xdeathstate',
    'deathsound',
    'speed',
    'radius',
    'height',
    'displayoffset',
    'mass',
    'damage',
    'activesound',
    'flags',
    'raisestate'
]

input_file = open('../../STJr/SRB2/src/info.c', 'r')
mobj_file = open('mobj_tables.txt', 'w')
states_file = open('state_tables.txt', 'w')

def read_mobj_info(line):
    name = line.split('//',1)[1].strip()
    attributes = []
    while True:
        line = input_file.readline().split(',',1)[0].split('//',1)[0].strip()
        if line == '}':
            break
        attributes.append(line)
    assert len(attributes) == len(mobj_attribute_names), 'Mobj definition must have ' + len(mobj_attribute_names) + ' entries!'
    mobj_file.write('{{Object info\n')
    mobj_file.write('| ' + name + '|POSSA1| objectid = ' + name + '\n')
    mobj_file.write('| thingid = ' + ('\'\'none\'\'' if attributes[0] == '-1' else attributes[0])  + '\n')
    mobj_file.write('| spriteid = POSS\n')
    for i in range(1, len(mobj_attribute_names)):
        mobj_file.write('| ' + mobj_attribute_names[i] + ' = ' + attributes[i].replace('|', '<nowiki>|</nowiki>') + '\n')
    mobj_file.write('}}\n')

def read_mobj_tables():
    line = input_file.readline().strip()
    assert line == '{', 'Expected {'
    while True:
        line = input_file.readline().strip()
        if line == '};':
            break
        if not line:
            continue
        if line[0] == '{':
            read_mobj_info(line)

def write_state_table_header():
    states_file.write('{| class="wikitable collapsible collapsed" style="width: 100%;"\n')
    states_file.write('! colspan="9"|States\n')
    states_file.write('|-\n')
    states_file.write('!State\n')
    states_file.write('!Sprite prefix\n')
    states_file.write('!Frame\n')
    states_file.write('!Frame flags\n')
    states_file.write('!Duration (tics)\n')
    states_file.write('![[Actions|Action]]\n')
    states_file.write('!Var1\n')
    states_file.write('!Var2\n')
    states_file.write('!Next state\n')

def extract_frame_flags(attributes):
    #A few states (foolishly!) use addition instead of bitwise OR to apply flags
    #Treat addition as bitwise OR anyway and hope nothing breaks
    frame_components = attributes[1].replace(' + ', '<nowiki>|</nowiki>').split('<nowiki>|</nowiki>')
    frame_value = 'A'
    frame_flags_list = []
    for x in frame_components:
        try:
            frame_value = chr(ord('A') + int(x))
        except ValueError:
            frame_flags_list.append(x)
    frame_flags = "<nowiki>|</nowiki>".join(frame_flags_list)
    attributes[1] = frame_value
    attributes.insert(2, frame_flags)
    return attributes

def read_state_tables():
    line = input_file.readline().strip()
    assert line == '{', 'Expected {'
    write_state_table_header()
    while True:
        line = input_file.readline().strip()
        if not line:
            continue
        if line == '};':
            break
        line, name = [x.strip() for x in line.split('//',1)]
        if not line:
            continue
        name = name.split(',',1)[0].split('//',1)[0].strip()
        states_file.write('|-\n')
        states_file.write('|' + name + '\n')
        attributes = line[1:-2].split(',')
        attributes = [x.strip().replace('|', '<nowiki>|</nowiki>').replace('<<', '<nowiki><<</nowiki>').replace('-', '<nowiki>-</nowiki>') for x in attributes]
        attributes = extract_frame_flags(attributes)
        attributes[4] = '\'\'none\'\'' if attributes[4] == '{NULL}' else '[[' + attributes[4][1:-1] + ']]'
        for x in attributes:
            states_file.write('|' + x + '\n')
    states_file.write('|}\n')

while True:
    line = input_file.readline()
    if not line:
        break
    line = line.strip()
    if line == 'mobjinfo_t mobjinfo[NUMMOBJTYPES] =':
        read_mobj_tables()
    elif line == 'state_t states[NUMSTATES] =':
        read_state_tables()

input_file.close()
mobj_file.close()
states_file.close()