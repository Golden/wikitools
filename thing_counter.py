import glob, sys

thing_names = ['' for x in range(4096)]
thing_categories = []
ring_values = { 300: 1, 400: 10, 600: 5, 601: 5, 602: 5, 603: 10, 604: 8, 605: 16, 608: 4, 609: 8, 1800: 1 }
rings_category = -1

def parse_thing_names():
    with open('thing_names.txt', 'r') as name_file:
        for line in name_file:
            line = line.strip()
            if not line:
                continue
            if line.startswith('=='):
                thing_categories.append([line.strip('='), []])
            else:
                type, name = line.strip().split(': ', 1)
                type = int(type.strip('* Thing type '))
                if type == 300:
                    global rings_category
                    rings_category = thing_categories[-1][0]
                thing_names[type] = name
                thing_categories[-1][1].append(type)

def parse_things(map, data, lump_start, lump_size):
    thing_count = [0 for x in range(4096)]
    
    for i in range(lump_size//10):
        index = lump_start + i*10
        type = int.from_bytes(data[index+6:index+8], "little", signed=False)
        thing_count[type % 4096] += 1
        
    total_rings = 0
    for type, value in ring_values.items():
        total_rings += value * thing_count[type]
    
    with open(map + '.txt', 'w') as output_file:
        output_file.write('{| class="wikitable collapsible collapsed"\n')
        output_file.write('|-\n')
        output_file.write('! colspan="2" | [[Thing types|Things]]\n')
                        
        for [name, types] in thing_categories:
            header_written = False
            for type in types:
                if thing_count[type] == 0:
                    continue
                if not header_written:
                    output_file.write('|-\n')
                    output_file.write('! [[Thing types#' + name + '|' + name + ']]\n')
                    output_file.write('! Occurrences\n')
                    header_written = True
                output_file.write('|-\n')
                output_file.write('| ' + thing_names[type] + '\n')
                output_file.write('| ' + str(thing_count[type]) + '\n')
            if header_written and name == rings_category:
                output_file.write('|-\n')
                output_file.write('| \'\'\'Total rings\'\'\'\n')
                output_file.write('| ' + str(total_rings) + '\n')
        output_file.write('|}\n')
    
def read_wad(map):
    print('Parsing ' + map + '...')
    with open(map + '.wad', 'rb') as input_file:
        header_size = 12
        type = input_file.read(4)
        assert type == b'PWAD', 'Input file is not a WAD file with PWAD header!'
        num_lumps = int.from_bytes(input_file.read(4), "little")
        directory_offset = int.from_bytes(input_file.read(4), "little")
        data = input_file.read(directory_offset - header_size)

        for i in range(num_lumps):
            lump_start = int.from_bytes(input_file.read(4), "little") - header_size
            lump_size = int.from_bytes(input_file.read(4), "little")
            lump_name = input_file.read(8).decode('UTF-8').rstrip('\0')
            if lump_name == 'THINGS':
                parse_things(map, data, lump_start, lump_size)
                break
    
parse_thing_names()

if len(sys.argv) < 2:
    #Parse all WAD files in the directory
    for file in glob.glob("*.wad"):
        read_wad(file.rstrip('.wad'))
else:
    #Parse specified files
    for i in range(1, len(sys.argv)):
        read_wad(sys.argv[i])